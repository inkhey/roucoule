
### Pre-commit

---

To enforce most required guidelines, please use the precommit mechanism.
This will perform some automatic checks before committing.

To use it, you need the `pre-commit` Python
you can then install hooks with:

```shell script
pip install pre-commit commitizen
pre-commit install
pre-commit install --hook-type commit-msg
```

Then, you will be able to notice the automatic checks when running `git commit`.

To run pre-commit on all files:

```shell script
pre-commit run --all-files
```

### Release

To use it, you need the `release` plugin for cargo:

```shell script
cargo install cargo-release
```

```shell script
cargo 
```