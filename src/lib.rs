#![doc = include_str!("../README.md")]
use glob::glob;
use image::imageops::overlay;
use image::io::Reader as ImageReader;
use image::{ImageBuffer, RgbaImage};
use std::path::PathBuf;

/// Splitter char for layers CLI Args
const LAYER_SPLIT_CHAR: &str = ",";

pub struct AvatarGenerator {
    pub output_path: PathBuf,
    pub theme_path: PathBuf,
    pub layers: String,
    pub width: u32,
    pub height: u32,
}

impl AvatarGenerator {
    pub fn generate_avatar(&self, slug: &str) {
        let mut img: RgbaImage = ImageBuffer::new(self.width, self.height);
        for layer in &mut self.layers.split(LAYER_SPLIT_CHAR) {
            let mut files: Vec<PathBuf> = vec![];
            let pattern = format!("{}/{layer}_[0-9]*.png", &self.theme_path.display());
            let pattern = pattern.as_str();
            for entry in glob(pattern).expect("Failed to read glob pattern") {
                match entry {
                    Ok(path) => files.push(path),
                    Err(e) => println!("{:?}", e),
                }
            }
            let selected_file = files.clone().choose_one(slug);
            let new_img = ImageReader::open(&selected_file)
                .expect("Fail to open image")
                .decode()
                .expect("Fail to decode image");
            log::debug!("{}", selected_file.display());
            overlay(&mut img, &new_img, 0, 0)
        }
        img.save(&self.output_path).expect("error saving image");
    }
}

/// A trait for making object able to return part of it according to slug
trait Choosable<T: Clone> {
    /// Choose one item
    fn choose_one(&self, slug: &str) -> T;
}

impl<T: Clone> Choosable<T> for Vec<T> {
    fn choose_one(&self, slug: &str) -> T {
        // this is some code to convert slug to an almost constant integer.
        let mut unique_id: usize = 0;
        for byte in slug.as_bytes() {
            unique_id = unique_id.overflowing_add(*byte as usize).0;
        }
        let length = self.len();
        let mut index = unique_id % length;
        index = index.saturating_sub(1);
        self[index].clone()
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_choose_one() {
        let a = vec!['a', 'b', 'c', 'd', 'e'];
        assert_eq!(a.choose_one("foo"), 'd');
        assert_eq!(a.choose_one("roucoule"), 'c');
        assert_eq!(a.choose_one("bar"), 'd');
        assert_eq!(a.choose_one("test"), 'c');
        assert_eq!(a.choose_one("éléphant"), 'a');
    }
}
